from optparse import OptionParser


class Alchemist:
    def __init__(self, filename, resnum, resname, ffbonded, rtp, charge_file=None, types_file=None, switch=False):
        """
        alch = Alchemist('file.itp', 10)
        """
        self.filename = filename
        self.deprot = {"ASH": "ASP", "GLH": "GLU", "GLU": "GLU", "ASP": "ASP", "HSP": "HSD", "GLUP": "GLU",
                       "ASPP": "ASP", "LYS": "LYN"}
        self.resnum = resnum
        self.resname = resname
        self.switch = switch
        self.rtp = rtp
        self.ffbonded = [l for l in open(ffbonded)]
        self.content = [line for line in open(self.filename)]
        self.sections, self.sections_names = self.get_sections()
        self.names_to_types_A, self.nums_to_names, self.names_to_nums, self.nums_to_types_other = self.get_types()
        if charge_file and types_file:
            self.charges = {line.split()[0]: line.split()[1] for line in open(charge_file)}
            self.names_to_types_B = self.parse_types(types_file)
        else:
            self.charges, self.names_to_types_B = self.parse_rtp()
        self.different_types = self.find_different_types()
        self.prefix = 'alch-'
        # TODO enable swapping states A and B with a single flag variable
    
    def __call__(self):
        """
        alch()
        """
        if self.switch:
            self.swap_states_ab()
        else:
            self.add_state_b()
        self.all_sections_add_b()
        self.write()
    
    def parse_rtp(self):
        # read and return charges & names_to_types
        with open(self.rtp, 'r') as f:
            lista = []
            x = 0
            for line in f:
                if len(line.split()) > 1 and line.split()[0] == '[' and line.split()[1] == self.deprot[self.resname]: 
                    x = 1
                if x == 1 and line.split()[0] != '[':
                    lista.append(line)
                if len(line.split()) > 1 and line.split()[0] == '[' and line.split()[1] == 'bonds' and x == 1:
                    break
        charges_dict = {}
        name_types_dict = {}
        for line in lista:
            name_types_dict[line.split()[0]] = line.split()[1]
            charges_dict[line.split()[0]] = line.split()[2]
        return charges_dict, name_types_dict
    
    def get_types(self):
        # return a dict of names:types in state A
        # self.names_to_types_A, self.nums_to_names, self.names_to_nums = self.get_types()
        section_sel = [l for l in self.get_section_content('atoms') if len(l.split()) > 5 and
                       l.split()[2] == str(self.resnum) and
                       not l.lstrip().startswith('[') and not l.lstrip().startswith(';')]
        section_whole = [l for l in self.get_section_content('atoms') if len(l.split()) > 5 and
                         not l.lstrip().startswith('[') and not l.lstrip().startswith(';')]
        natta = {l.split()[4]: l.split()[1] for l in section_sel}
        ntna = {l.split()[0]: l.split()[4] for l in section_sel}
        natn = {l.split()[4]: l.split()[0] for l in section_sel}
        ntta = {l.split()[0]: l.split()[1] for l in section_whole}
        return natta, ntna, natn, ntta
    
    def find_different_types(self):
        diffs = []
        for num in self.names_to_nums.values():
            name = self.nums_to_names[num]
            try:
                if self.names_to_types_A[name] != self.names_to_types_B[name]:
                    diffs.append(num)
            except KeyError:
                diffs.append(num)
        return diffs
    
    @staticmethod
    def parse_types(types_file):
        # nazwa atomu pierwsza kolumna, typ druga kolumna
        types = [line for line in open(types_file)]
        return {line.split()[0]: line.split()[1] for line in types if len(line.split()) > 1}
    
    def get_sections(self):
        # petla po content
        # dwie listy
        # jesli zaczyna sie od '[', to nazwa do listy z nazwami, a pusta lista do listy z sekcjami
        # dodajemy kolejne linijki do ostatniej podlisty
        names, section_insides = [], []
        section_insides.append([])
        names.append('header')
        for line in self.content:
            if line.strip().startswith('['):
                names.append(line.split()[1])
                section_insides.append([])
            section_insides[-1].append(line)
        return section_insides, names
    
    def get_section_content(self, section_name, order=0):
        # znalezc wszystkie instancje nazwy "section_name" w self.sections_names
        # ustalic, o ktora chodzi (order) -> numer sekcji w self.sections
        # zwrocic odpowiedni element self.sections
        n = len(self.sections_names)
        indegz = [j for j in range(n) if self.sections_names[j] == section_name][order]
        return self.sections[indegz]
     
    def set_section_content(self, section_name, order, line_num, new_line):
        n = len(self.sections_names)
        indegz = [j for j in range(n) if self.sections_names[j] == section_name][order]
        self.sections[indegz][line_num] = new_line
    
    def add_state_b(self):
        section_content = self.get_section_content('atoms')
        # w petli po liniach:
        # (1) sprawdzic, czy linia jest legitna
        # (2) znalezc typ, ladunek i mase
        # (3) dodac w odp kolejnosci do linii
        # (4) zapisac zmodyfikowana linie w odpowiednim elemencie self.sections
        num = 0
        for linenum, line in enumerate(section_content):
            if len(line.split()) > 7 and not line.lstrip().startswith(';') and str(line.split()[2]) == str(self.resnum):
                nazwa, typ, masa = line.split()[4], line.split()[1], line.split()[7]
                try:
                    ladunek = self.charges[nazwa]
                except KeyError:
                    ladunek = "0.00"
                if nazwa in self.names_to_types_B.keys():
                    typ = self.names_to_types_B[nazwa]
                else:
                    typ = 'DH'
                    self.names_to_types_B[nazwa] = 'DH'
                line = line.rstrip()
                if ';' in line:
                    line = line[:line.index(';')]
                suffix = "{:10s} {:10s} {:10s}".format(typ, ladunek, masa)
                new_line = line + suffix + '\n'
                self.set_section_content('atoms', 0, linenum, new_line)
                num += 1
    
    def swap_states_ab(self):
        section_content = self.get_section_content('atoms')
        num = 0
        for linenum, line in enumerate(section_content):
            if len(line.split()) > 7 and not line.lstrip().startswith(';') and str(line.split()[2]) == str(self.resnum):
                nazwa, typ_a, masa_a, lad_a = line.split()[4], line.split()[1], line.split()[7], line.split()[6]
                try:
                    lad_b = self.charges[nazwa]
                except KeyError:
                    lad_b = "0.00"
                if nazwa in self.names_to_types_B.keys():
                    typ_b = self.names_to_types_B[nazwa]
                else:
                    typ_b = 'DH'
                    self.names_to_types_B[nazwa] = 'DH'
                ls = line.split()
                form = "{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11s}{:>11s}{:>10s}{:>11s}{:>11s}\n"
                new_line = form.format(ls[0], typ_b, ls[2], ls[3], nazwa, ls[5], lad_b, masa_a, typ_a, lad_a, masa_a)
                self.set_section_content('atoms', 0, linenum, new_line)
                num += 1
    
    def all_sections_add_b(self):
        self.add_b_to_section('bonds', 2)
        self.add_b_to_section('angles', 3)
        self.add_b_to_section('dihedrals', 4)
        self.add_b_to_section('dihedrals', 4, 1)
    
    def add_b_to_section(self, section, n_entries, section_order=0):
        section_content = self.get_section_content(section, section_order)
        for linenum, line in enumerate(section_content):
            atomnum_list = line.split()[0:n_entries]
            if any([True for i in atomnum_list if i in self.different_types]):
                types_a, types_b = [], []
                for l in atomnum_list:
                    try:
                        types_a.append(self.names_to_types_A[self.nums_to_names[l]])
                        types_b.append(self.names_to_types_B[self.nums_to_names[l]])
                    except KeyError:
                        types_a.append(self.nums_to_types_other[l])
                        types_b.append(self.nums_to_types_other[l])
                if n_entries == 4:
                    if section_order == 0:
                        par_a, par_b = self.find_param(types_a, {'9'}), self.find_param(types_b, {'9'})
                    else:
                        par_a, par_b = self.find_param(types_a, {'2', '4'}), self.find_param(types_b, {'2', '4'})
                else:
                    par_a, par_b = self.find_param(types_a), self.find_param(types_b)
                if par_a is None:
                    raise RuntimeError('{} not found in ffbonded'.format(' '.join(types_a)))
                if par_b is None:
                    par_b = par_a
                if self.switch:
                    par_b, par_a = par_a, par_b
                if len(par_b[0]) == len(par_a[0]) == 3:
                    par_a, par_b = self.check_dihedrals(par_a, par_b)
                for qa, qb in zip(par_a, par_b):
                    if section_order == 1:
                        print(qa, qb)
                    form = "{:10} "*len(qa)*2
                    suffix = form.format(*qa, *qb)
                    new_line = line.rstrip() + suffix + '\n'
                    self.set_section_content(section, section_order, linenum, new_line)
        
    @staticmethod
    def check_dihedrals(pa, pb):
        all_multis = sorted(list({p[2] for p in pa + pb}))
        pa_new = []
        pb_new = []
        for q in all_multis:
            try:
                a = [x for x in pa if x[2] == q][0]
            except IndexError:
                a = [0.0, 0.0, q]
            try:
                b = [x for x in pb if x[2] == q][0]
            except IndexError:
                b = [0.0, 0.0, q]
            pa_new.append(a)
            pb_new.append(b)
        return pa_new, pb_new
        
    def find_param(self, atomtypes_list, query_parmtype={'1', '5'}):
        nparams = len(atomtypes_list)
        interaction_types = ['1', '2', '4', '5', '9']
        param = []
        wilds = []
        for line in self.ffbonded[::-1]:
            lspl = line.split()
            if len(lspl) > nparams and lspl[nparams] in interaction_types and lspl[nparams] in query_parmtype:
                cond, wild = self.test_line(lspl[:nparams], atomtypes_list)
                if cond:
                    if ';' not in lspl[nparams+1:]:
                        param.append([float(x) for x in lspl[nparams+1:]])
                    else:
                        endpos = lspl.index(';')
                        param.append([float(x) for x in lspl[nparams+1:endpos]])
                    wilds.append(wild)
                    if len(param[-1]) == 3:
                        param[-1][-1] = int(param[-1][-1])
        multiple_dihs = True if len(wilds) < len(param) - 1 else False
        if multiple_dihs:
            return [param[x] for x in range(len(param)) if wilds[x] == 0]
        elif param:
            return param[-1:]
        else:
            return None
    
    @staticmethod
    def test_line(line_list, ref_list):
        assert len(line_list) == len(ref_list)
        wildcard = 0
        if 'X' in line_list:
            non_x_nums = [x for x in range(len(line_list)) if line_list[x] != "X"]
            line_list = [line_list[a] for a in non_x_nums]
            ref_list = [ref_list[a] for a in non_x_nums]
            wildcard = 1
        n = len(line_list)
        if all([line_list[i] == ref_list[i] for i in range(n)]) or \
                all([line_list[i] == ref_list[n-i-1] for i in range(n)]):
            return True, wildcard
        else:
            return False, wildcard
    
    def write(self):
        with open(self.prefix + self.filename, 'w') as outfile:
            for section in self.sections:
                for line in section:
                    outfile.write(line)


def parse_options():
    parser = OptionParser(usage="%prog -p itp_file -n residue_number -r resname -b ffbonded -d rtp_file [--switch]")
    parser.add_option("-p", dest="itpfile", action="store", type="string",
                      help="topology file to be modified (.itp)")
    parser.add_option("-n", dest="resnum", action="store", type="int",
                      help="number of the residue to be modified")
    parser.add_option("-r", dest="resname", action="store", type="string",
                      help="name of the residue to be modified")
    parser.add_option("-b", dest="ffb", action="store", type="string",
                      help="path to the appropriate ffbonded file in gromacs database")
    parser.add_option("-d", dest="rtp", action="store", type="string",
                      help="path to the appropriate rtp file in gromacs database")
    parser.add_option("--switch", dest="switch", action="store_true",
                      help="switches states A and B")
    (options, args) = parser.parse_args()
    return options
    
    
opts = parse_options()
q = Alchemist(opts.itpfile, opts.resnum, opts.resname, opts.ffb, opts.rtp, switch=opts.switch)
q()
